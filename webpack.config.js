const fs = require("fs");
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const sourcePath = path.resolve(__dirname, './src');
const entryPoints = fs.readdirSync(sourcePath)
    .filter(fileName => /\.js$/.test(fileName))
    .reduce((acc, fileName) => {
        acc[fileName.replace('.js', '')] = path.join(__dirname, '/src/', fileName);
        return acc;
    }, {});

module.exports = {
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    externals: {
        'aws-sdk': 'aws-sdk'
    },
    mode: 'development',
    entry: entryPoints,
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new CleanWebpackPlugin()
    ],
    output: {
        path: path.join(__dirname, "build"),
        library: "[name]",
        libraryTarget: "commonjs2",
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.json$/,
                exclude: /node_modules/,
                loaders: ['json']
            }
        ]
    }
}