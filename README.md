## Introduction

Assessment project for Vopak as AWS serverless api.

* Lambda
* ApiGateway
* Deployment with Cloudformation

* gitlab CI/CD Pipeline

* Nodejs
* Jensk testing
* Data storage in MongoDB

## SetUp
    git clone git@gitlab.com:djverlinden/vopak.git

# Run :
    yarn install
    yarn test
    yarn build


gitlab CiCd pipeline variables:

    MONGO_HOST
    MONGO_CREDENTIALS
    AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY

git push origin/master will trigger the CI

## ToDo


* Security
* Error handling
* Integration test in the pipeline
* Better unit tests
* Database access secrets in AWS Secret Manager

---

## Project is tested/build on MacOS with:
* yarn: v1.22.4
* node: v12.00.0