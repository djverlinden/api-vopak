const Connection = require('../src/models/Connection');

class ConnectionApi {
    constructor() {
        this.connection = new Connection();
    }

    close() {
        return this.connection.close();
    }

    connect() {
        console.dir(this.connection.DB_NAME)
        return this.connection.connection();
    }
}

jest.mock('../src/models/Connection');

beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    Connection.mockClear();
});

it('check connection', () => {
    const connectionApi = new ConnectionApi();
    const connection = new Connection();

    // connectionApi.connect()
    // connectionApi.close()
});