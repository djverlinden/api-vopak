const TempertureMeasurement = require('../src/models/TempertureMeasurement');

class TempertureMeasurementApi {
    constructor() {
        this.temperturemeasurement = new TempertureMeasurement();
    }

    setValue() {
        const obj = { 'city' : 'leiden', 'temperature' : '21' };
        return this.temperturemeasurement.setModel(obj);
    }
}

jest.mock('../src/models/TempertureMeasurement');

beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    TempertureMeasurement.mockClear();
});

it('check values', () => {
    const tempertureMeasurementApi = new TempertureMeasurementApi();
    const obj = { 'city' : 'leiden', 'temperature' : '21' }
    tempertureMeasurementApi.setValue(obj)

    const mockTempertureMeasurement = TempertureMeasurement.mock.instances[0];
    const mockObj= mockTempertureMeasurement.setModel;

    expect(mockObj.mock.calls[0][0]).toEqual(obj);

    expect(mockObj).toHaveBeenCalledWith(obj);
    expect(mockObj).toHaveBeenCalledTimes(1);
});