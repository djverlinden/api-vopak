const CurrentTempService = require('../src/controllers/CurrentTempService');

class CurrentTempServiceApi {
    constructor() {
        this.currentTempService = new CurrentTempService();
    }

    response() {
        return this.currentTempService.loadTestData();
    }
}

jest.mock('../src/controllers/CurrentTempService');

beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    CurrentTempService.mockClear();
});

it('check values', () => {
    const currentTempServiceApi = new CurrentTempServiceApi();
    currentTempServiceApi.response()

    // const mockTempertureMeasurement = TempertureMeasurement.mock.instances[0];
    // const mockObj= mockTempertureMeasurement.setModel;

    // expect(mockObj.mock.calls[0][0]).toEqual(obj);

    // expect(mockObj).toHaveBeenCalledWith(obj);
    // expect(mockObj).toHaveBeenCalledTimes(1);
});