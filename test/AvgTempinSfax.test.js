const AvgTempinSfax = require('../src/controllers/AvgTempinSfax');

class AvgTempinSfaxApi {
    constructor() {
        this.avgTempinSfax = new AvgTempinSfax();
    }

    response() {
        return this.avgTempinSfax.response();
    }
}

jest.mock('../src/controllers/AvgTempinSfax');

beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    AvgTempinSfax.mockClear();
});

it('check values', () => {
    const avgTempinSfaxApi = new AvgTempinSfaxApi();
    const avgTempinSfax = new AvgTempinSfax();
    let responseValue = avgTempinSfaxApi.response()

    const mockAvgTempinSfax = AvgTempinSfax.mock.instances[0];
    const mockPlaySoundFile = mockAvgTempinSfax.response;

    expect(mockPlaySoundFile.mock.calls[0][0]).toEqual(responseValue);
});


it('errors', () => {
});