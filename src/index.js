'use strict';

const controllers = {
    currenttempincovilha : require ('../src/controllers/CurrentTempService'),
    avgtempinsfax : require('../src/controllers/AvgTempinSfax')
}

let defaultReponse = {
      statusCode: 502,
      body: false,
      isBase64Encoded: false,
      headers: {
          'Cache-Control': 'no-cache',
          'Accept': 'application/json'
      }
}

exports.handler = async ( event, context ) => {
    context.callbackWaitsForEmptyEventLoop = false;
    let service     = event.path.replace(/^\/|\/$/g, '')
    let controller  = new controllers[ service ](event, defaultReponse);
    return await controller.response() || defaultReponse;
};
