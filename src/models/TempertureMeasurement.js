'use strict';

const Connection = require('../../src/models/Connection')

module.exports = class TempertureMeasurement extends Connection {
    constructor() {
        super('temperature', 'measurement')
        this.data = [];
    }

    /**
     * set values in model
     */
    setModel( { city, temperature, date } ){

        console.dir( date )
        date = date || new Date()

        return this.data.push({
            'city' : city,
            'date' : new Date(date),
            'temperature' : temperature
        });
    }

    /**
     * insert value in collection
     */
    create( ){
        return new Promise( async (resolve) => {
            await super.connection()

            await this.db.collection(this.COLLECTION).insertMany(this.data);
            await super.close()

            resolve( JSON.stringify( this.data ) )
        })

    }

    async getAvg( { city, from, to } ){

        return new Promise( async (resolve) => {

            await super.connection()

            const result = await this.db.collection(this.COLLECTION).aggregate([
                {
                    $match: {
                        city: city, date: {$gte: from, $lt: to}
                    }
                },
                {$group: {_id: null, average: {$avg: "$temperature"}}},
                {$project: {_id: 0, average: 1}}
            ]).toArray();

            await super.close();

            resolve( +(result[0].average.toFixed(2)) )
        })
    }

}