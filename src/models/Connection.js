'use strict';

const MongoClient = require('mongodb').MongoClient;

const MONGO_CONNECTION_URI = '@cluster0-yjkxj.mongodb.net/test?retryWrites=true&w=majority'
const MONGO_USER = 'vopak'
const MONGO_PASWORD = '$$Ea1anZ0L&vEjUd58w'

module.exports = class Connection {
    constructor(db_name, collection) {
        this.client = false
        this.COLLECTION = collection
        this.DB_NAME = db_name
        this.data = [ 'CREATED' ];
        this.url = 'mongodb+srv://'+MONGO_USER+':'+MONGO_PASWORD+MONGO_CONNECTION_URI;
    }
    /**
     * create connection
     */
    async connection(){
        this.client = this.client || await this.getClient();
        this.db = this.client.db(this.DB_NAME);
    }

    /**
     * close connection
     */
    async close(){
        await this.client.close();
    }

    /**
     * get MongoClient connection
     */
    getClient (){
        return MongoClient.connect( this.url , {useUnifiedTopology: true})
            .catch(err => { console.log(err); });
    }
}