'use strict';

const Controller    = require('./Controller');
const Model         = require('../../src/models/TempertureMeasurement');

module.exports = class AvgTempinSfax extends Controller{
   async response() {
        let model = new Model()
        this.reponse.body = JSON.stringify( {
            AverageTemperature : await model.getAvg( {
                from: new Date(Date.UTC(2019, 5, 1)),
                to: new Date(Date.UTC(2019, 5, 25)),
                city:'sfax'
            })
        });

        return this.reponse;
   }
}