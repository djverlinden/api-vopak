'use strict';
const Controller    = require('./Controller');
const Model         = require('../../src/models/TempertureMeasurement');
const https         = require('https');
const url           = "https://printwerk_verlinden:KcaQ4s7dWEr0H@api.meteomatics.com/"
const parameters    = "t_2m:C"
const format        = "json"

module.exports = class currentTempService extends Controller {
   /*
    * Get and save data to db
    */
    async response(){
        this.tstd = false
        this.model      = new Model()
        let city        = this.getCity(this.body);
        let temperature = this.getTemperature(this.body);

        // load data into the model
        // if emty test data set input data
        this.tstd || this.model.setModel( { city:city, temperature:temperature, date:false }  )

    	try {
            this.reponse.body       = await this.model.create();
            this.reponse.statusCode = 200
    	}
    	catch(error) {
    		// Promise rejected
            this.reponse.body       = JSON.stringify( error );
            this.reponse.statusCode = 400
    	}

        return this.reponse;
    }

    getTemperature( { temperature }  ){
        return temperature || this.loadTestData();
    }

    getCity( { city }  ){
        return city || 'covilha'
    }

   /*
    * Get data from meteomatics
    */
   loadTestData(){
            // raw data, Limit(s) for user exceeded. Too many queries in period Day.
        const load = JSON.parse( '{"data":[{"coordinates":[{"dates":[{"date":"2020-05-06T12:00:00Z","value":19.0},{"date":"2020-05-06T13:00:00Z","value":19.0},{"date":"2020-05-06T14:00:00Z","value":19.2},{"date":"2020-05-06T15:00:00Z","value":19.1},{"date":"2020-05-06T16:00:00Z","value":19.2},{"date":"2020-05-06T17:00:00Z","value":19.1},{"date":"2020-05-06T18:00:00Z","value":19.0},{"date":"2020-05-06T19:00:00Z","value":19.0},{"date":"2020-05-06T20:00:00Z","value":19.1},{"date":"2020-05-06T21:00:00Z","value":19.2},{"date":"2020-05-06T22:00:00Z","value":19.4},{"date":"2020-05-06T23:00:00Z","value":19.5},{"date":"2020-05-07T00:00:00Z","value":19.5},{"date":"2020-05-07T01:00:00Z","value":19.0},{"date":"2020-05-07T02:00:00Z","value":18.5},{"date":"2020-05-07T03:00:00Z","value":18.5},{"date":"2020-05-07T04:00:00Z","value":18.6},{"date":"2020-05-07T05:00:00Z","value":18.6},{"date":"2020-05-07T06:00:00Z","value":18.5},{"date":"2020-05-07T07:00:00Z","value":18.4},{"date":"2020-05-07T08:00:00Z","value":18.5},{"date":"2020-05-07T09:00:00Z","value":18.7},{"date":"2020-05-07T10:00:00Z","value":18.8},{"date":"2020-05-07T11:00:00Z","value":18.9},{"date":"2020-05-07T12:00:00Z","value":19.1}]}]}]}' );
        let days = 1;
        load.data[0].coordinates[0].dates.forEach( element => {
            this.model.setModel( { city:'sfax', temperature:element.value, date:`2019-06-${days}` } )
            days++;
        });

        return this.tstd = true;

        // lookup latitude/longitude covilha
        // needed for the test data
        this.latitude      = 40.28601
        this.longitude     = 7.50396

        this.url = url + new Date().toISOString().slice(0,10) +  "T12ZP1D:PT1H/" + parameters  + "/" + this.latitude + "," + this.longitude + "/" + format

        return new Promise((resolve, reject) => {
    		https.get(this.url, (response) => {
    			let data = '';
                // response.setEncoding('utf8');
    			response.on('data', (chunk) => {
    				data += chunk;
    			});

    			response.on('end', () => {
                    let body = JSON.parse(data);
    			    resolve( body );
                });

    			response.on('error', (error) => {
    				reject(error);
    			});
    		});
    	});
   }
}