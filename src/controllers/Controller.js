'use strict';

module.exports = class Controller {
   constructor( event, defaultReponse ) {
      console.dir(event)
        this.event      = event
        this.body       = JSON.parse(event.body)
        this.reponse    = defaultReponse
        this.statusCode = 200
   }

}